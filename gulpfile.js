const { src, dest, series, watch } = require('gulp');
const sass = require('gulp-sass')(require('sass'));

function copy_js() {
    return src('input/*.js')
        .pipe(dest('template/js/'));
}

function build_styles() {
    return src('input/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('template/css/'));
}

function images() {
    return src('input/*.{png,svg}')
        .pipe(dest('template/images/'));
}

function fonts() {
    return src('input/*/*.ttf')
        .pipe(dest('template/'));
}

exports.copy_js = copy_js();
exports.build_styles = build_styles();
exports.images = images();
exports.fonts = fonts();
exports.default = series(copy_js, build_styles, images, fonts);